# stat-utils

> `Utils.Stat` -- statistics utilities

- `Utils.Stat.Primitive` -- top-level primitive types and functions
- `Utils.Stat.Summary` -- summary statistics
- `Utils.Stat.Estimator` -- estimators
- `Utils.Stat.Process` -- stochastic processes
- `Utils.Stat.Signal` -- signal generators and statistical signal processing
