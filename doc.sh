#!/bin/sh

set -x

cabal haddock && firefox dist-newstyle/build/x86_64-linux/ghc-*/stat-utils-*/doc/html/stat-utils/index.html

exit 0
