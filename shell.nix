{ nixpkgs ? import <nixpkgs> { config.allowBroken = true; },
  compiler ? "ghc8107",
  doBenchmark ? false
}:

let
  inherit (nixpkgs) pkgs;
  f = { mkDerivation, base, basic-prelude, Chart, Chart-cairo, classy-prelude
      , clock, colour, chart-utils, hmatrix, linear, mtl, mwc-random, random
      , random-fu, stdenv, storable-tuple, vector
      }:
      mkDerivation {
        pname = "stat-utils";
        version = "0.1.0.0";
        src = ./.;
        isLibrary = true;
        isExecutable = true;
        libraryHaskellDepends = [
          base basic-prelude chart-utils hmatrix linear mtl random random-fu
          storable-tuple vector
        ] ++ mydevtools;
        executableHaskellDepends = [
          base Chart Chart-cairo classy-prelude clock colour hmatrix random-fu
        ] ++ mydevtools;
        license = stdenv.lib.licenses.bsd3;
      };
  haskellPackages = if compiler == "default" then pkgs.haskellPackages
    /*
    # fix for random-fu, random-source
    then pkgs.haskellPackages.extend (self: super: {
      random-fu = pkgs.haskell.lib.doJailbreak super.random-fu;
      random-source = pkgs.haskell.lib.doJailbreak super.random-source;
    })
    */
    else pkgs.haskell.packages.${compiler};
  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;
  drv = variant (haskellPackages.callPackage f {
    inherit chart-utils;
  });

  # nix-prefetch-git --url https://gitlab.com/spearman/chart-utils-hs.git
  /*
  chart-utils = haskellPackages.callCabal2nix "chart-utils" "${(pkgs.fetchgit {
    url = https://gitlab.com/spearman/chart-utils-hs.git;
    rev = "cb4558d39b4957523e447edfd19d2eb01abb0f45";
    sha256 = "1my5664hivch48fghrqfl6ba4c8iwsr7ca9bg06d7wqypndi9rcl";
  })}" {};
  */
  chart-utils = haskellPackages.callCabal2nix "chart-utils" ../utils-chart {};

  mydevtools = [
    haskellPackages.cabal-install
    haskellPackages.ghc
    haskellPackages.ghcide
    # ghc-prof broken
    #haskellPackages.profiterole
    #haskellPackages.profiteur
  ];

in
  if pkgs.lib.inNixShell then drv.env else drv
