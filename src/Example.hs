{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import ClassyPrelude hiding (Vector, (<>))

--import Control.Monad  (forM)
import Data.Int       (Int16)
import Data.Word      (Word16)
import Text.Printf    (printf)

import Data.Vector.Storable          (Vector, (!))
import Numeric.LinearAlgebra         (Matrix)
import Graphics.Rendering.Chart.Easy (def, (.=))

import qualified Data.Colour.Names                         as Colour
import qualified Data.Random                               as Random
import qualified Data.Random.Distribution.Bernoulli as Distribution.Bernoulli
import qualified Data.Random.Distribution.T         as Distribution.T
import qualified Data.Vector.Storable                      as Vector
import qualified Data.Vector                               as Boxed
import qualified Graphics.Rendering.Chart.Easy             as Chart
import qualified Graphics.Rendering.Chart.Backend.Cairo    as Chart.Cairo
import qualified System.Clock                              as Clock

import qualified Utils.Chart                               as Chart.Utils

import Utils.Stat (
  Expected (..), Observed (..), Parameter (..), inner)
import qualified Utils.Stat.Estimator as Estimator
import qualified Utils.Stat.Process   as Process
import qualified Utils.Stat.Signal    as Signal
import qualified Utils.Stat.Summary   as Summary

import Utils.Stat.Signal (Signal (..))

(!!) :: (IsSequence seq) => seq -> Index seq -> Element seq
(!!) = unsafeIndex

main :: IO ()
main = do
  putStrLn "stat-utils example main..."

  let sample_size = 10000
  let interval    = Vector.fromList [0.0, 100.0]
  let center      = (interval!0 + interval!1) / 2.0
  let mean'       = Parameter center
  let stddev      = Parameter (10.0 :: Double)
  let expected    = Expected $ Vector.replicate sample_size center
  x <- map (Observed . Vector.fromList)
    $ forM [1..sample_size] $ const $ Random.sample
      $ Random.Normal (inner mean') (inner stddev)

  putStrLn "~~~ normal distribution ~~~"
  printf "mean:                        %8.3f\n" (inner mean'  :: Double)
  printf "standard deviation:          %8.3f\n" (inner stddev :: Double)
  printf "sample size:                     %4i\n"   sample_size
  --
  -- primitives
  --
  printf "mean deviation:              %8.3f\n"
    (inner $ Summary.deviation_mean expected x :: Double)
  printf "mean abs deviation:          %8.3f\n"
    (inner $ Summary.deviation_abs_mean  expected x :: Double)
  printf "mean squared deviation:      %8.3f\n"
    (inner $ Summary.deviation_squared_mean expected x :: Double)
  printf "root mean squared deviation: %8.3f\n"
    (inner $ Summary.deviation_squared_mean_root expected x :: Double)
  printf "deviation magnitude:         %8.3f\n"
    (inner $ Summary.deviation_magnitude expected x :: Double)
  --
  -- summary statistics
  --
  printf "sample mean:                 %8.3f\n"
    (inner $ Summary.sample_mean x :: Double)
  printf "sample variance:             %8.3f\n"
    (inner $ Summary.sample_variance x :: Double)
  printf "zeroth raw moment:           %8.3f\n"
    (inner $ Summary.sample_moment_raw 0 x :: Double)
  printf "zeroth central moment:       %8.3f\n"
    (inner $ Summary.sample_moment_central 0 x :: Double)
  printf "first central moment:        %8.3f\n"
    (inner $ Summary.sample_moment_central 1 x :: Double)

  --
  -- histogram estimator
  --
  putStrLn "~~~ histogram ~~~"
  let bins   = 256
  let _bins64 = fromIntegral bins :: Int64
  let _binvec = Estimator.histogram_bins bins (interval!0, interval!1) x
  let counts = Estimator.histogram_counts bins (interval!0, interval!1) x
  -- graph
  let values = Vector.map fromIntegral counts
  Chart.Utils.plot_bars_sequence "histogram.png"
    (Just Chart.Utils.layout_axes_hidden) values

  Chart.Utils.plot_histogram_normalized "hist.png"
    (Just Chart.Utils.layout_axes_hidden) (inner x) 100

  --
  -- sample joint IID veriables
  --
  putStrLn "~~~ joint distribution ~~~"
  printf "sample size:                     %4i\n"   sample_size
  let sample_size = 1000
  points <- Process.sample (Distribution.T.T 3 :: Distribution.T.T Double)
    sample_size
  Chart.Utils.plot_points "joint.png" Nothing [points]

  --
  -- sample random vectors with correlation
  --
  putStrLn "~~~ random vectors ~~~"
  let correlation_matrix
        = Process.mk_correlation_matrix 2 (Vector.fromList [0.5])
        :: Matrix Double
  let sample_size = 1000
  printf "sample size:                     %4i\n"   sample_size
  points <- Process.sample
    (Process.RandomVector
      2
      (Distribution.T.T 3 :: Distribution.T.T Double)
      correlation_matrix
    ) sample_size
    :: IO (Boxed.Vector (Vector Double))
  Chart.Utils.plot_points "correlation.png" Nothing
    [(Vector.convert $ Boxed.map (\v -> (v!0, v!1)) points)]

  --
  -- bernoulli process
  --
  putStrLn "~~~ bernoulli process ~~~"
  let sample_size = 256
  let probabilities@(probability1:probability2:probability3:[])
        = [0.5, 0.1, 0.8] :: [Double]
  printf "sample size:                     %4i\n"   sample_size
  printf "probability1:                %8.3f\n" probability1
  printf "probability2:                %8.3f\n" probability2
  printf "probability3:                %8.3f\n" probability3
  samples <- forM probabilities $ \probability -> fmap Observed $ Process.sample
    (Distribution.Bernoulli.Bernoulli probability
      :: Distribution.Bernoulli.Bernoulli Double Double) sample_size
  let estimated = fmap Summary.sample_mean samples
  printf "estimated1:                  %8.3f\n" (inner $ estimated!!0 :: Double)
  printf "estimated2:                  %8.3f\n" (inner $ estimated!!1 :: Double)
  printf "estimated3:                  %8.3f\n" (inner $ estimated!!2 :: Double)
  -- graph
  let values  = fmap (\sample ->
        zip [0.0..fromIntegral sample_size :: Double]
          $ fmap (:[]) $ Vector.toList (inner sample :: Vector Double)) samples
  let layouts = fmap (\bars ->
        Chart.StackedLayout $ Chart.execEC $ do
          Chart.Utils.layout_axes_hidden
          Chart.layout_y_axis . Chart.laxis_generate
            .= Chart.scaledAxis def (0.0, 1.0)
          Chart.plot $ fmap Chart.plotBars $ Chart.liftEC $ do
            Chart.Utils.style_bars_solid Colour.blue
            Chart.plot_bars_values .= bars)
        values
  Chart.Cairo.toFile (def { Chart.Cairo._fo_format = Chart.Cairo.PNG })
    "bernoulli.png" $ Chart.slayouts_layouts .= layouts

  --
  -- lattice random walks
  --
  putStrLn "~~~ lattice random walk ~~~"
  let sample_size = 256
  samples <- Process.sample (Process.simple_walk_symmetric 1) sample_size
    :: IO (Vector Int)
  -- graph
  let values = Vector.map fromIntegral samples
  Chart.Utils.plot_bars_sequence "walk.png" (Just Chart.Utils.layout_axes_hidden)
    values

  --
  -- fractional brownian bridge
  --
  putStrLn "~~~ fractional brownian bridge ~~~"
  let sample_size = 1000

  putStrLn "dependence index = 0.5"
  t1 <- Clock.getTime Clock.Monotonic
  !samples <- Process.sample (Process.brownian_bridge 0.5) sample_size
    :: IO (Vector Double)
  t2 <- Clock.getTime Clock.Monotonic
  printf "elapsed (ms): %f\n"
    $ (fromIntegral $ Clock.toNanoSecs $ t2 - t1 :: Double) / 10e5
  -- graph
  Chart.Utils.plot_bars_sequence "bbridge-50.png"
    (Just Chart.Utils.layout_axes_hidden) samples

  putStrLn "dependence index = 0.15"
  t1 <- Clock.getTime Clock.Monotonic
  !samples <- Process.sample (Process.brownian_bridge 0.15) sample_size
  t2 <- Clock.getTime Clock.Monotonic
  printf "elapsed (ms): %f\n"
    $ (fromIntegral $ Clock.toNanoSecs $ t2 - t1 :: Double) / 10e5
  -- graph
  Chart.Utils.plot_bars_sequence "bbridge-15.png"
    (Just Chart.Utils.layout_axes_hidden) samples

  putStrLn "dependence index = 0.95"
  t1 <- Clock.getTime Clock.Monotonic
  !samples <- Process.sample (Process.brownian_bridge 0.95) sample_size
  t2 <- Clock.getTime Clock.Monotonic
  printf "elapsed (ms): %f\n"
    $ (fromIntegral $ Clock.toNanoSecs $ t2 - t1 :: Double) / 10e5
  -- graph
  Chart.Utils.plot_bars_sequence "bbridge-95.png"
    (Just Chart.Utils.layout_axes_hidden) samples

  --
  -- fractional cauchy bridge
  --
  putStrLn "~~~ fractional cauchy bridge ~~~"
  let sample_size = 1000

  putStrLn "dependence index = 0.5"
  t1 <- Clock.getTime Clock.Monotonic
  !samples <- Process.sample (Process.cauchy_bridge 0.5) sample_size
    :: IO (Vector Double)
  t2 <- Clock.getTime Clock.Monotonic
  printf "elapsed (ms): %f\n"
    $ (fromIntegral $ Clock.toNanoSecs $ t2 - t1 :: Double) / 10e5
  -- graph
  Chart.Utils.plot_bars_sequence "cbridge-50.png"
    (Just Chart.Utils.layout_axes_hidden) samples

  putStrLn "dependence index = 0.15"
  t1 <- Clock.getTime Clock.Monotonic
  !samples <- Process.sample (Process.cauchy_bridge 0.15) sample_size
  t2 <- Clock.getTime Clock.Monotonic
  printf "elapsed (ms): %f\n"
    $ (fromIntegral $ Clock.toNanoSecs $ t2 - t1 :: Double) / 10e5
  -- graph
  Chart.Utils.plot_bars_sequence "cbridge-15.png"
    (Just Chart.Utils.layout_axes_hidden) samples

  putStrLn "dependence index = 0.95"
  t1 <- Clock.getTime Clock.Monotonic
  !samples <- Process.sample (Process.cauchy_bridge 0.95) sample_size
  t2 <- Clock.getTime Clock.Monotonic
  printf "elapsed (ms): %f\n"
    $ (fromIntegral $ Clock.toNanoSecs $ t2 - t1 :: Double) / 10e5
  -- graph
  Chart.Utils.plot_bars_sequence "cbridge-95.png"
    (Just Chart.Utils.layout_axes_hidden) samples

  --
  --  levy staircase
  --
  putStrLn "~~~ levy staircase ~~~"
  let sample_size = 256

  putStrLn "shape parameter = 0.3"
  !samples <- Process.sample (Process.levy_staircase 0.3) sample_size
  -- graph
  Chart.Utils.plot_line_fill "staircase-30.png"
    (Just Chart.Utils.layout_axes_hidden) samples

  putStrLn "shape parameter = 0.63093"
  !samples <- Process.sample (Process.levy_staircase $ log 2 / log 3) sample_size
  -- graph
  Chart.Utils.plot_line_fill "staircase-63.png"
    (Just Chart.Utils.layout_axes_hidden) samples

  putStrLn "shape parameter = 0.9"
  !samples <- Process.sample (Process.levy_staircase 0.9) sample_size
  -- graph
  Chart.Utils.plot_line_fill "staircase-90.png"
    (Just Chart.Utils.layout_axes_hidden) samples


  --
  -- noise signals
  --
  putStrLn "~~~ uniform white noise ~~~"
  let sample_size = 256
  Signal samples
    <- Signal.generate (Signal.noise_white_uniform @Int16) sample_size
    :: IO (Signal Int16)
  -- graph
  let values = Vector.map fromIntegral samples
  Chart.Utils.plot_bars_sequence "uniform.png"
    (Just Chart.Utils.layout_axes_hidden) values

  putStrLn "~~~ gaussian normal white noise ~~~"
  let sample_size = 256
  Signal signal <- Signal.generate (Signal.noise_white_gaussian_normal_int64
    $ fromIntegral (maxBound :: Word16) - 1) sample_size
    :: IO (Signal Int64)
  -- convert to Int16 and normalize
  let Signal samples = Signal.peak_normalize_int $ Signal
        $ Vector.map fromIntegral signal
        :: Signal Int16
  -- graph
  let values = Vector.map fromIntegral samples
  Chart.Utils.plot_bars_sequence "gaussian.png"
    (Just Chart.Utils.layout_axes_hidden) values

  putStrLn "...stat-utils example main"
