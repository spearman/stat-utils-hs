-- | Statistics utilities

module Utils.Stat (
  module Utils.Stat,
  module Utils.Stat.Primitive,
  -- * Modules
  module Utils.Stat.Estimator,
  module Utils.Stat.Process,
  module Utils.Stat.Signal,
  module Utils.Stat.Summary
) where

import Utils.Stat.Primitive -- contents of unqualified import are re-exported
import qualified Utils.Stat.Estimator
import qualified Utils.Stat.Process
import qualified Utils.Stat.Signal
import qualified Utils.Stat.Summary
