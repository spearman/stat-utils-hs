-- | Statistical signal processing and noise generation

{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Utils.Stat.Signal (
  Signal    (..),
  Noise     (..),
  Generator (..),
  noise_white_uniform,
  noise_white_gaussian_normal_int64,
  peak_center_int,
  peak_normalize_int
) where

import BasicPrelude hiding (Vector, max)

--import Debug.Trace

import Data.Vector.Storable (Vector)

import qualified Data.Vector.Storable as Vector
import qualified Data.Random                       as Random
import qualified Data.Random.Distribution.Binomial as Distribution.Binomial
import qualified Data.Random.Distribution.Uniform  as Distribution.Uniform

import Utils.Stat.Process (Process (..))
import qualified Utils.Stat.Process as Process

-- | A signal is a vector of samples
newtype Signal s = Signal (Vector s)
  deriving (Show)

-- | Noise signal parameters
data Noise p s = Noise {
  process   :: p,
  offset    :: s,
  amplitude :: s
} deriving (Show)

-- | Signal generators
class Generator generator sample where
  -- | Generate a signal of a given size using an IO means of randomness
  generate :: generator -> Word32 -> IO (Signal sample)
  -- | Generate a signal of a given size from a given Int seed
  generate_seed :: generator -> Word32 -> Int -> Signal sample

-- | Noise signal generator
instance (Num s, Vector.Storable s, Process p (Vector s))
  => Generator (Noise p s) s where
  generate :: Noise p s -> Word32 -> IO (Signal s)
  generate (Noise process offset amplitude) length
    = map (Signal . (Vector.map $ \x -> amplitude * (x+offset)))
      $ Process.sample process length
  generate_seed :: Noise p s -> Word32 -> Int -> Signal s
  generate_seed (Noise _process _offset _amplitude) _length _seed = undefined

-- | Uniform white noise with offset of 0 and amplitude 1
noise_white_uniform :: (
  Num s, Bounded s, Vector.Storable s,
  Random.Distribution Distribution.Uniform.Uniform s)
  => Noise (Distribution.Uniform.Uniform s) s
noise_white_uniform = Noise (Distribution.Uniform.Uniform minBound maxBound) 0 1

-- | Integer-valued gaussian normal white noise centered at zero with amplitude
-- 1
noise_white_gaussian_normal_int64 :: Int64
  -> Noise (Distribution.Binomial.Binomial Double Int64) Int64
noise_white_gaussian_normal_int64 trials
  = Noise (Distribution.Binomial.Binomial trials 0.5) offset 1 where
    offset = fromIntegral $ -trials `div` 2

-- | Center a signed integral-valued signal
peak_center_int :: forall s. (Bounded s, Integral s, Storable s)
  => Signal s -> Signal s
peak_center_int (Signal signal) = Signal $ Vector.map (+shift) signal where
  max   = realToFrac $ Vector.maximum signal :: Double
  min   = realToFrac $ Vector.minimum signal :: Double
  shift = round $ -(max + min) / 2.0 :: s

-- | Normalize a signed integral-valued signal
--
-- NOTE: this might not work for Int64 signals
peak_normalize_int :: forall s. (Bounded s, Integral s, Storable s)
  => Signal s -> Signal s
peak_normalize_int (Signal signal)
  = Signal $ Vector.map (round . (*scale) . realToFrac) signal where
    scale      = int_max / signal_max :: Double
    int_max    = realToFrac (maxBound :: s)
    signal_max = realToFrac $ Vector.maximum $ Vector.map abs signal
