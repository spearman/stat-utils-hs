-- | Statistics primitives

{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_HADDOCK hide #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Utils.Stat.Primitive (
  -- * Types
  Probability (..),
  Population (..),
  Observed (..),
  Expected (..),
  Estimated (..),
  Error (..),
  Deviation (..),
  Residual (..),
  Parameter (..),
  Statistic (..),
  -- * Classes
  Inner (..),
  -- * Functions
  mean,
  error,
  residual,
  deviation,
  deviation_from,
  deviation_abs,
  deviation_abs_from,
  deviation_squared,
  deviation_squared_from
) where

import BasicPrelude hiding (Vector, error)

import qualified Data.Vector.Storable as Vector
import Data.Vector.Storable  (Vector)
import Numeric.LinearAlgebra () -- Num instances for vectors

-- | A normalized probability represented as a double-precision floating point
-- value in the range [0.0, 1.0]
newtype Probability = Probability Double
  deriving (Show)

-- | A population is the set of all observable units; not a valid dataset for
-- inference, but can be used to compute population parameters
newtype Population  = Population (Vector Double)
  deriving (Show)
-- | An observed random variate, e.g. a random sample or realized experimental
-- outcome
newtype Observed    = Observed   (Vector Double)
  deriving (Show)

-- | Either a theoretical or computed expected value
newtype Expected    = Expected   (Vector Double)
  deriving (Show)
-- | A statistical estimate
newtype Estimated   = Estimated  (Vector Double)
  deriving (Show)

-- | An error represents the difference between an estimate and the true
-- (expected) value
newtype Error       = Error      (Vector Double)
  deriving (Show)
-- | A deviation represents the difference between an observed value an the true
-- (expected) value
newtype Deviation   = Deviation  (Vector Double)
  deriving (Show)
-- | A residual is the difference between an observed value and a predicted
-- value
newtype Residual    = Residual   (Vector Double)
  deriving (Show)

-- | A parameter is any value computed from a population
newtype Parameter t = Parameter t
  deriving (Show)
-- | A statistic is any value computed from a random sample
newtype Statistic t = Statistic t
  deriving (Show)

-- | A convenience class for converting newtype wrappers into inner values
class Inner t v where
  inner :: t -> v
instance Inner Probability Double where
  inner (Probability x) = x
instance Inner Population (Vector Double) where
  inner (Population x) = x
instance Inner Observed (Vector Double) where
  inner (Observed x) = x
instance Inner Expected (Vector Double) where
  inner (Expected x) = x
instance Inner Estimated (Vector Double) where
  inner (Estimated x) = x
instance Inner Error (Vector Double) where
  inner (Error x) = x
instance Inner Deviation (Vector Double) where
  inner (Deviation x) = x
instance Inner Residual (Vector Double) where
  inner (Residual x) = x
instance Inner (Parameter t) t where
  inner (Parameter x) = x
instance Inner (Statistic t) t where
  inner (Statistic x) = x

--
--  population parameters
--

-- | Arithmetic mean; see sample_mean for the summary statistic
mean :: Population -> Parameter Double
mean (Population population) = Parameter
  $ (Vector.sum population)
    / (fromIntegral $ Vector.length population)

--
--  descriptive statistics
--

-- | Difference between expected and estimated values
error :: Expected -> Estimated -> Error
error (Expected expected) (Estimated estimated) = Error $ estimated - expected

-- | Difference between estimated and observed values
residual :: Estimated -> Observed -> Residual
residual (Estimated estimated) (Observed observed)
  = Residual $ observed - estimated

-- | Difference between expected and observed
deviation :: Expected -> Observed -> Deviation
deviation (Expected expected) (Observed observed)
  = Deviation $ observed - expected

-- | Absolute deviation
deviation_abs :: Expected -> Observed -> Observed
deviation_abs expected observed = Observed $ Vector.map abs d where
  Deviation d = deviation expected observed

-- | Squared deviation
deviation_squared :: Expected -> Observed -> Observed
deviation_squared expected observed = Observed $ Vector.map (**2) d
  where Deviation d = deviation expected observed

-- | Signed deviation from central value
deviation_from :: Parameter Double -> Observed -> Deviation
deviation_from (Parameter center) observed@(Observed sample) = deviation
  (Expected $ Vector.replicate (Vector.length sample) center) observed

-- | Absolute deviation from central value
deviation_abs_from :: Parameter Double -> Observed -> Observed
deviation_abs_from (Parameter center) observed@(Observed sample) = deviation_abs
  (Expected $ Vector.replicate (Vector.length sample) center) observed

-- | Squared deviation from central value
deviation_squared_from :: Parameter Double -> Observed -> Observed
deviation_squared_from (Parameter center) observed@(Observed sample)
  = Observed $ Vector.map (**2) d where
    Deviation d = deviation
      (Expected $ Vector.replicate (Vector.length sample) center) observed
