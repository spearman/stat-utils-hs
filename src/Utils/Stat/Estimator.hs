-- | Estimators

{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeApplications #-}

module Utils.Stat.Estimator (
  -- * Density estimation
  histogram_bins,
  histogram_counts
) where

import BasicPrelude hiding (Vector)

import Data.Int (Int64)

import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vector

import Utils.Stat.Primitive (Observed (..))

-- | Assign observations among N bins in the given interval
histogram_bins :: Int -> (Double, Double) -> Observed -> Vector Int64
histogram_bins bins interval (Observed sample)
  = Vector.map (\x -> floor $ (x - (fst interval)) / binwidth) sample
    where binwidth = ((snd interval) - (fst interval)) / (fromIntegral bins)

-- | Count of observations in each of N bins in the given inteval
histogram_counts :: Int -> (Double, Double) -> Observed -> Vector Int64
histogram_counts bins interval (Observed sample) = rec counts'
  $ Vector.toList sample where
    range'              = (snd interval) - (fst interval)
    binwidth            = range' / (fromIntegral bins)
    counts'             = Vector.replicate bins 0
    rec counts []       = counts
    rec counts (x : xs) = rec (counts + indicator) xs where
      indicator = Vector.fromList @Int64
        $ (take bin' $ repeat 0) ++ (1 : (take (bins-bin'-1) $ repeat 0))
      bin'      = floor $ (x - (fst interval)) / binwidth
