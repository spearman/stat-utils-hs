-- | Stochastic processes

{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Utils.Stat.Process (
  Process            (..),
  -- * Discrete processes
  RandomVector       (..),
  SimpleWalk         (..),
  BrownianFractional,
  BrownianBridge     (..),
  CauchyFractional,
  CauchyBridge       (..),
  LevyStaircase      (..),
  brownian,
  brownian_bridge,
  brownian_fractional,
  cauchy_bridge,
  cauchy_flight,
  cauchy_fractional,
  dependence_index,
  dependence_index',
  levy_staircase,
  simple_walk_symmetric,
  -- * Helper functions
  -- TODO: move these to a different module?
  iid_sequence,
  mk_correlation_matrix
) where

import BasicPrelude hiding (Vector, (<.>))

import Data.Vector.Storable       (Vector, (!), (!?))
import Foreign.Storable.Tuple     ()
import Linear                     (V1(..), V2(..))
import Numeric.LinearAlgebra      ((#>))
import Numeric.LinearAlgebra.Data (Matrix)

import qualified Control.Monad.State                  as State
import qualified Data.Vector.Storable                 as Vector
import qualified Data.Vector                          as Boxed
import qualified Data.Random                          as Random
import qualified Data.Random.Distribution.Categorical as Distribution.Categorical
import qualified Data.Random.Distribution.Pareto      as Distribution.Pareto
import qualified Data.Random.Distribution.Normal      as Distribution.Normal
import qualified Data.Random.Distribution.T           as Distribution.T
import qualified Numeric.LinearAlgebra                as LinearAlgebra
import qualified System.Random

-- | Generic interface for stochastic processes
class Process process sample where
  -- | Generate a sample of a given size using an IO means of randomness
  sample :: process -> Word32 -> IO sample
  -- | Generate a sample of a given size from a given @Int@ seed
  sample_seed :: process -> Word32 -> Int -> sample

-- | Defines a sequence of IID random vectors
data RandomVector d v = RandomVector {
  dimension          :: Word32,
  -- ^ Dimension of the vector space
  distribution       :: d,
  -- ^ Distribution of individual component variables
  correlation_matrix :: Matrix v
  -- ^ Symmetric matrix of correlation coefficients
} deriving (Show)

-- | A simple random walk on a discrete lattice where each step is to a
-- neighboring site
data SimpleWalk = SimpleWalk (Vector (Double, Double))
  -- ^ An n-dimensional simple walk defined by weights for (positive, negative)
  -- steps in each dimension
  deriving (Show)

-- | Fractional Brownian motion
data BrownianFractional v = BrownianFractional {
  dependence_index :: v -- ^ Hurst exponent in range (0, 1)
} deriving (Show)

-- | Brownian bridge process
data BrownianBridge v = BrownianBridge (BrownianFractional v)
  deriving (Show)

-- | Fractional Cauchy flight
data CauchyFractional v = CauchyFractional {
  dependence_index' :: v -- ^ Hurst exponent in range (0, 1)
} deriving (Show)

-- | Cauchy bridge process
data CauchyBridge v = CauchyBridge (CauchyFractional v)
  deriving (Show)

-- | Levy staircase function
data LevyStaircase v = LevyStaircase {
  shape_parameter :: v
  -- ^ Shape parameter of the Pareto distribution used for the length of the
  -- steps taken in the abscissa
} deriving (Show)

-- | An unbiased random walk in the given dimension
simple_walk_symmetric :: Int -> SimpleWalk
simple_walk_symmetric dimension
  = SimpleWalk $ Vector.replicate dimension (1.0, 1.0)

-- | Fractional Brownian motion with the given index of long-range dependence
-- (Hurst exponent). It is an error if the dependence index is outside of the
-- open interval (0, 1).
brownian_fractional :: Double -> BrownianFractional Double
brownian_fractional h = if h <= 0.0 || h >= 1.0 then
  error $ "dependence index out of range (0, 1): " ++ show h
else BrownianFractional h

-- | Standard Brownian motion (Wiener process) is a fractional Brownian process
-- with index of long-range dependence @H=0.5@
brownian :: BrownianFractional Double
brownian = BrownianFractional 0.5

-- | Brownian bridge process is a fractional Brownian process where the
-- endpoints are fixed to zero
brownian_bridge :: Double -> BrownianBridge Double
brownian_bridge h = BrownianBridge $ BrownianFractional h

-- | Fractional Cauchy flight with the given index of long-range dependence
-- (Hurst exponent). It is an error if the dependence index is outside of the
-- open interval (0, 1).
cauchy_fractional :: Double -> CauchyFractional Double
cauchy_fractional h = if h <= 0.0 || h >= 1.0 then
  error $ "dependence index out of range (0, 1): " ++ show h
else CauchyFractional h

-- | Standard cauchy flight with index of long-range dependence @H=0.5@
cauchy_flight :: CauchyFractional Double
cauchy_flight = CauchyFractional 0.5

-- | Cauchy bridge process is a fractional Cauchy flight where the
-- endpoints are fixed to zero
cauchy_bridge :: Double -> CauchyBridge Double
cauchy_bridge h = CauchyBridge $ CauchyFractional h

-- | Levy staircase is a randomized Cauchy staircase function ("devil's
-- staircase") with shape parameter determining the Pareto distribution of step
-- lengths in the function's abscissa
levy_staircase :: Double -> LevyStaircase Double
levy_staircase = LevyStaircase

-- | Generic impl for process consisting of IID samples from any
-- 'Random.Distribution'
instance {-# OVERLAPPABLE #-} (
  Vector.Storable v,
  Random.Distribution d v)
  => Process (d v) (Vector v)
  where
    sample :: d v -> Word32 -> IO (Vector v)
    sample = iid_sequence
    sample_seed :: d v -> Word32 -> Int -> Vector v
    sample_seed distribution size seed
      = State.evalState (iid_sequence distribution size)
        $ System.Random.mkStdGen seed

-- | Generic impl for process consisting of joint IID samples from any
-- 'Random.Distribution'
instance {-# OVERLAPPABLE #-} (
  Vector.Storable v,
  Random.Distribution d v)
  => Process (d v) (Vector (v, v))
  where
    sample :: d v -> Word32 -> IO (Vector (v, v))
    sample distribution size = do
      s1 <- iid_sequence distribution size
      s2 <- iid_sequence distribution size
      return $ Vector.zipWith (,) s1 s2
    sample_seed :: d v -> Word32 -> Int -> Vector (v, v)
    sample_seed _distribution _size _seed
      = undefined -- TODO

instance (
  LinearAlgebra.Numeric v,
  Random.Distribution d v)
  => Process (RandomVector (d v) v) (Boxed.Vector (Vector v)) where
  sample :: RandomVector (d v) v -> Word32 -> IO (Boxed.Vector (Vector v))
  sample (RandomVector dimension distribution correlation_matrix) size =
    Boxed.replicateM (fromIntegral size) $ do
      s <- iid_sequence distribution dimension
      return $ correlation_matrix #> s
  sample_seed :: RandomVector (d v) v -> Word32 -> Int
    -> Boxed.Vector (Vector v)
  sample_seed = undefined -- TODO

instance Process SimpleWalk (Vector Int) where
  sample :: SimpleWalk -> Word32 -> IO (Vector Int)
  sample (SimpleWalk weights) size = Vector.iterateNM (fromIntegral size)
    (\x -> map (+x) $ Random.sample distribution) 0
    where
      distribution = Distribution.Categorical.categorical [
        ((or_zero $ map fst $ weights!?0) / total_weight,  1),
        ((or_zero $ map snd $ weights!?0) / total_weight, -1) ]
      total_weight = Vector.sum $ Vector.map (uncurry (+)) weights
  sample_seed :: SimpleWalk -> Word32 -> Int -> Vector Int
  sample_seed (SimpleWalk weights) size seed = State.evalState
    (Vector.iterateNM (fromIntegral size)
      (\x -> map (+x) $ Random.sample distribution) 0)
    $ System.Random.mkStdGen seed
    where
      distribution = Distribution.Categorical.categorical [
        ((or_zero $ map fst $ weights!?0) / total_weight,  1),
        ((or_zero $ map snd $ weights!?0) / total_weight, -1) ]
      total_weight = Vector.sum $ Vector.map (uncurry (+)) weights

instance Process SimpleWalk (Vector (V1 Int)) where
  sample :: SimpleWalk -> Word32 -> IO (Vector (V1 Int))
  sample (SimpleWalk weights) size = Vector.iterateNM (fromIntegral size)
    (\x -> map (+x) $ Random.sample distribution) 0
    where
      distribution = Distribution.Categorical.categorical [
        ((or_zero $ map fst $ weights!?0) / total_weight,  1),
        ((or_zero $ map snd $ weights!?0) / total_weight, -1) ]
      total_weight = Vector.sum $ Vector.map (uncurry (+)) weights
  sample_seed :: SimpleWalk -> Word32 -> Int -> Vector (V1 Int)
  sample_seed (SimpleWalk weights) size seed = State.evalState
    (Vector.iterateNM (fromIntegral size)
      (\x -> map (+x) $ Random.sample distribution) 0)
    $ System.Random.mkStdGen seed
    where
      distribution = Distribution.Categorical.categorical [
        ((or_zero $ map fst $ weights!?0) / total_weight,  1),
        ((or_zero $ map snd $ weights!?0) / total_weight, -1) ]
      total_weight = Vector.sum $ Vector.map (uncurry (+)) weights

instance Process SimpleWalk (Vector (V2 Int)) where
  sample :: SimpleWalk -> Word32 -> IO (Vector (V2 Int))
  sample (SimpleWalk weights) size = Vector.iterateNM (fromIntegral size)
    (\x -> map (+x) $ Random.sample distribution) 0
    where
      distribution = Distribution.Categorical.categorical [
        ((or_zero $ map fst $ weights!?0) / total_weight, V2   1   0 ),
        ((or_zero $ map snd $ weights!?0) / total_weight, V2 (-1)  0 ),
        ((or_zero $ map fst $ weights!?1) / total_weight, V2   0   1 ),
        ((or_zero $ map snd $ weights!?1) / total_weight, V2   0 (-1)) ]
      total_weight = Vector.sum $ Vector.map (uncurry (+)) weights
  sample_seed :: SimpleWalk -> Word32 -> Int -> Vector (V2 Int)
  sample_seed (SimpleWalk weights) size seed = State.evalState
    (Vector.iterateNM (fromIntegral size)
      (\x -> map (+x) $ Random.sample distribution) 0)
    $ System.Random.mkStdGen seed
    where
      distribution = Distribution.Categorical.categorical [
        ((or_zero $ map fst $ weights!?0) / total_weight, V2   1   0 ),
        ((or_zero $ map snd $ weights!?0) / total_weight, V2 (-1)  0 ),
        ((or_zero $ map fst $ weights!?1) / total_weight, V2   0   1 ),
        ((or_zero $ map snd $ weights!?1) / total_weight, V2   0 (-1)) ]
      total_weight = Vector.sum $ Vector.map (uncurry (+)) weights

-- | Construction of fractional Brownian motion by integration of fractional
-- gaussian noise (Cholesky method, Asmussen 1998); reference:
-- <https://github.com/crflynn/fbm>. This method tested to be more than 5x
-- faster than the method of computing the square root of the covariance matrix
-- using eigenvectors.
instance Process (BrownianFractional Double) (Vector Double) where
  sample :: BrownianFractional Double -> Word32 -> IO (Vector Double)
  sample process size = do
    gaussian_vector <- Vector.replicateM noise_size
      $ Random.sample Distribution.Normal.StdNormal :: IO (Vector Double)
    return $ Vector.scanl (+) 0.0 $ dependency gaussian_vector
    where
      dependency = if h == 0.5 then id else (stddev_matrix #>)
      stddev_matrix
        = LinearAlgebra.chol $ LinearAlgebra.trustSym autocovariance_matrix
      autocovariance_matrix = lower + identity + upper where
        lower    = LinearAlgebra.build dims $ \i j -> if j < i
          then let k = i - j in
            0.5 * ((abs $ k-1)**(2*h) - 2*(abs k)**(2*h) + (abs $ k+1)**(2*h))
          else 0.0
        identity = LinearAlgebra.ident noise_size
        upper    = LinearAlgebra.tr lower
      dims = (noise_size, noise_size) :: (Int, Int)
      h    = dependence_index process
      noise_size = fromIntegral $ size-1
  sample_seed :: BrownianFractional Double -> Word32 -> Int -> Vector Double
  sample_seed = undefined  -- TODO

instance {-# OVERLAPPING #-} Process (BrownianBridge Double) (Vector Double) where
  sample :: BrownianBridge Double -> Word32 -> IO (Vector Double)
  sample (BrownianBridge process) size = do
    brownian_motion <- sample process size
    return $ Vector.imap (\t x ->
      let tfrac = (fromIntegral t) / (fromIntegral $ size - 1) in
      x - tfrac * Vector.last brownian_motion)
      brownian_motion
  sample_seed :: BrownianBridge Double -> Word32 -> Int -> Vector Double
  sample_seed = undefined  -- TODO

-- | Construction of fractional Cauchy flight by integration of fractional
-- Cauchy noise (Cholesky method, Asmussen 1998); reference:
-- <https://github.com/crflynn/fbm>. This method tested to be more than 5x
-- faster than the method of computing the square root of the covariance matrix
-- using eigenvectors.
instance Process (CauchyFractional Double) (Vector Double) where
  sample :: CauchyFractional Double -> Word32 -> IO (Vector Double)
  sample process size = do
    cauchy_vector
      <- Vector.replicateM noise_size $ Random.sample $ Distribution.T.T 1
      :: IO (Vector Double)
    return $ Vector.scanl (+) 0.0 $ dependency cauchy_vector
    where
      dependency = if h == 0.5 then id else (stddev_matrix #>)
      stddev_matrix
        = LinearAlgebra.chol $ LinearAlgebra.trustSym autocovariance_matrix
      autocovariance_matrix = lower + identity + upper where
        lower    = LinearAlgebra.build dims $ \i j -> if j < i
          then let k = i - j in
            0.5 * ((abs $ k-1)**(2*h) - 2*(abs k)**(2*h) + (abs $ k+1)**(2*h))
          else 0.0
        identity = LinearAlgebra.ident noise_size
        upper    = LinearAlgebra.tr lower
      dims = (noise_size, noise_size) :: (Int, Int)
      h    = dependence_index'  process
      noise_size = fromIntegral $ size-1
  sample_seed :: CauchyFractional Double -> Word32 -> Int -> Vector Double
  sample_seed = undefined  -- TODO

instance {-# OVERLAPPING #-} Process (CauchyBridge Double) (Vector Double) where
  sample :: CauchyBridge Double -> Word32 -> IO (Vector Double)
  sample (CauchyBridge process) size = do
    cauchy_motion <- sample process size
    return $ Vector.imap (\t x ->
      let tfrac = (fromIntegral t) / (fromIntegral $ size - 1) in
      x - tfrac * Vector.last cauchy_motion)
      cauchy_motion
  sample_seed :: CauchyBridge Double -> Word32 -> Int -> Vector Double
  sample_seed = undefined  -- TODO

instance Process (LevyStaircase Double) (Vector (Double, Double)) where
  sample :: LevyStaircase Double -> Word32 -> IO (Vector (Double, Double))
  sample process size = do
    abcissa_steps <- Vector.replicateM (size_int-2) $ Random.sample
      $ Distribution.Pareto.Pareto ordinate_step $ shape_parameter process
    let xs  = Vector.scanl (+) 0.0 abcissa_steps
    let xs' = duplicate $ Vector.map (/ Vector.last xs) xs
    let ys  = Vector.iterateN (size_int-2) (+ordinate_step) ordinate_step
    let ys' = Vector.cons 0.0 $ Vector.snoc (duplicate ys) 1.0
    return $ Vector.zipWith (,) xs' ys'
    where
      size_int      = fromIntegral size :: Int
      ordinate_step = 1.0 / (fromIntegral $ size-1)
      duplicate     = Vector.concatMap (\x -> Vector.fromList [x, x])
  sample_seed :: LevyStaircase Double -> Word32 -> Int -> Vector (Double, Double)
  sample_seed = undefined  -- TODO

--
-- private
--

or_zero :: Maybe Double -> Double
or_zero = maybe 0.0 id

-- | Helper for IID sequence instance
iid_sequence :: (
  Vector.Storable v,
  Random.Distribution d v,
  Random.MonadRandom m) =>
  d v -> Word32 -> m (Vector v)
iid_sequence distribution size
  = Vector.replicateM (fromIntegral size) $ Random.sample distribution

-- | Input vector is a list of `(n*(n - 1))/2` correlation coefficients
-- [0.0, 1.0]. It is a fatal error if a coefficient is outside of this range.
mk_correlation_matrix :: (
  Ord v, RealFrac v, Num (Vector v), LinearAlgebra.Numeric v,
  LinearAlgebra.Container Vector v)
  => Int -> Vector v -> Matrix v
mk_correlation_matrix dimension correlations
  = lower + LinearAlgebra.ident dimension + upper
  where
    !() = if Vector.length correlations
        /= (dimension * (dimension - 1)) `div` 2
      then
        error "improper length of correlation vector"
      else ()
    upper = LinearAlgebra.build (dimension, dimension) $ \i j ->
      if i < j then get_correlation i j
      else fromInteger 0
    lower = LinearAlgebra.tr upper
    get_correlation i j
      = if coefficient < (fromInteger 0) || coefficient > (fromInteger 1) then
          error "coefficients must be between 0.0 and 1.0"
        else coefficient
      where
        coefficient = correlations ! (row_start x + (y - 1 - x))
        x = truncate i :: Int
        y = truncate j :: Int
        row_start :: Int -> Int
        row_start row = rec 0 dimension 0 where
          rec r n x = if r == row then x
            else rec (r+1) (n-1) (x + (n-1))
