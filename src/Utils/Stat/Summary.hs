-- | Summary statistics

{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Utils.Stat.Summary (
  -- * Sample moments
  sample_moment_raw,
  sample_moment_central,
  sample_moment_standard,
  sample_mean,
  sample_variance,
  sample_stddev,
  -- * Accuracy/precision measures
  deviation_sum,
  deviation_abs_sum,
  deviation_squared_sum,
  deviation_magnitude,
  deviation_mean,
  deviation_abs_mean,
  deviation_squared_mean,
  deviation_squared_mean_root
) where

import BasicPrelude hiding (Vector)

--import Debug.Trace

import qualified Data.Vector.Storable as Vector

import Numeric.LinearAlgebra (dot, norm_2)

import Utils.Stat.Primitive  (
  Deviation (..), Expected (..), Observed (..), Parameter (..), Statistic (..),
  deviation, deviation_abs, deviation_from, deviation_squared)

-- | $k$-th raw sample moment
sample_moment_raw :: Int -> Observed -> Statistic Double
sample_moment_raw order (Observed sample) = Statistic
  $ (Vector.sum $ Vector.map (**(fromIntegral order)) sample)
    / (fromIntegral $ Vector.length sample)

-- | $k$-th central sample moment
sample_moment_central :: Int -> Observed -> Statistic Double
sample_moment_central 0 _ = Statistic 1.0
sample_moment_central 1 _ = Statistic 0.0
sample_moment_central order observed@(Observed sample) = Statistic
  $ (Vector.sum $ Vector.map (**(fromIntegral order)) deviations)
    / (fromIntegral $ Vector.length sample) - 1
  where
    (Deviation deviations) = deviation_from (Parameter mean') observed
    (Statistic mean')      = sample_mean observed

-- | $k$-th standardized sample moment
sample_moment_standard :: Int -> Observed -> Statistic Double
sample_moment_standard 0 _ = Statistic 1.0
sample_moment_standard 1 _ = Statistic 0.0
sample_moment_standard 2 _ = Statistic 1.0
sample_moment_standard _order _sample = undefined -- TODO

-- | Sample mean (first raw moment)
sample_mean :: Observed -> Statistic Double
sample_mean = sample_moment_raw 1

-- | (Unbiased) sample variance (second central moment)
sample_variance :: Observed -> Statistic Double
sample_variance = sample_moment_central 2

-- | (Biased) sample standard deviation
sample_stddev :: Observed -> Statistic Double
sample_stddev observed = Statistic $ sqrt v
  where Statistic v = sample_variance observed

-- | Sum of signed deviations
deviation_sum :: Expected -> Observed -> Statistic Double
deviation_sum expected observed = Statistic $ Vector.sum d
  where Deviation d = deviation expected observed

-- | Sum of absolute deviations
deviation_abs_sum :: Expected -> Observed -> Statistic Double
deviation_abs_sum expected observed = Statistic $ Vector.sum d
  where Observed d = deviation_abs expected observed

-- | Sum of squared deviations
deviation_squared_sum :: Expected -> Observed -> Statistic Double
deviation_squared_sum expected observed = Statistic $ d `dot` d
  where Deviation d = deviation expected observed

-- | Magnitude of the signed deviation vector
deviation_magnitude :: Expected -> Observed -> Statistic Double
deviation_magnitude expected observed = Statistic $ norm_2 d
  where Deviation d = deviation expected observed

-- | Mean signed deviation
deviation_mean :: Expected -> Observed -> Statistic Double
deviation_mean expected observed = sample_mean (Observed d)
  where Deviation d = deviation expected observed

-- | Mean absolute deviation
deviation_abs_mean :: Expected -> Observed -> Statistic Double
deviation_abs_mean expected observed
  = sample_mean $ deviation_abs expected observed

-- | Biased variance (mean squared deviation)
deviation_squared_mean :: Expected -> Observed -> Statistic Double
deviation_squared_mean expected observed
  = sample_mean $ deviation_squared expected observed

-- | Doubly-biased standard deviation (square root of variance)
deviation_squared_mean_root :: Expected -> Observed -> Statistic Double
deviation_squared_mean_root expected observed = Statistic $ sqrt msd
  where Statistic msd = deviation_squared_mean expected observed
